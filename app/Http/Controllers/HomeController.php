<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
// use App\Repositories\Authentication\AuthenticationRepositoryInterface;
use App\Repositories\Category\CategoryInterface;
use App\Repositories\SubCategory\SubCategoryInterface;
use App\Repositories\Projects\ProjectInterface;

class HomeController extends Controller
{
    protected $authentication;
    protected $category;
    protected $subCategory;
    protected $project;

    public function __construct(
        // AuthenticationRepositoryInterface $authentication,
        CategoryInterface $category,
        SubCategoryInterface $subCategory,
        ProjectInterface $project
    ){
        // $this->authentication  = $authentication;
        $this->category        = $category;
        $this->subCategory     = $subCategory;
        $this->project         = $project;
    }

    public function checkUserRole(){
        if (Auth::user()){
            $roleID = Auth::user()->role;
//        Role ID 1 for admin, 2 for service seeker, 3 for service provider and 4 for consultant.
            if ($roleID === 1){
                return redirect('/admin');
            }elseif ($roleID === 2){
                return redirect('/user');
            }elseif ($roleID === 3){
                return redirect('/user');
            }elseif ($roleID === 4){
                return redirect('/consultant');
            }
        }else{
            redirect('/login');
        }

    }

    public function loadOnboardingPage() {
        return view('auth.onboard');
    }

    // public function validateLogin(Request $request) {
    //     $data = $request->all();
    //     $login = $this->authentication->login($data);
    //     if ($login['isSuccess'] == true){
    //         $this->checkUserRole();
    //     }else{

    //     }
    // }

    public function getActiveTasks(){
        $userId = Auth::id();
        $projects = $this->project->getUnAssignedProjects();
        return view('home.active_tasks', compact('projects'));
    }
}
