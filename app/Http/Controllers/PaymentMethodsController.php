<?php

namespace App\Http\Controllers;
use App\Repositories\PaymentMethods\PaymentMethodsInterface;
use Illuminate\Http\Request;
use Jleon\LaravelPnotify\Notify;

class PaymentMethodsController extends Controller
{
    protected $payment;
    public function __construct(
        PaymentMethodsInterface $payment
    ){
        $this->payment = $payment;
    }
    public function allPaymentMethodsList(){
        $payments = $this->payment->all();
        return view('admin.payment.all_payment_list', compact('payments'));
    }

    public function editPaymentMethod(Request $request){
        $data = $request->all();
        $payment = $this->payment->update($data);
        if ($payment['isSuccess'] == true){
            Notify::success($payment['message']);
        }else{
            Notify::error($payment['message']);
        }
        return redirect()->back();
    }
}
