<div class="sidebar">
    <div class="main-menu">
        <div class="scroll">
            <ul class="list-unstyled">
                <li class="active">
                    <a href="/home">
                        <i class="iconsmind-Shop-4"></i>
                        <span>Dashboards</span>
                    </a>
                </li>
                <li>
                    <a href="/user/tasks">
                        <i class="iconsmind-Star"></i>
                        <span>Tasks</span>
                    </a>
                </li>
                <li>
                    <a href="/user/account/{{Auth::id()}}">
                        <i class="iconsmind-Digital-Drawing"></i> Account
                    </a>
                </li>
                <li>
                    <a href="#chat">
                        <i class="iconsmind-Pantone"></i> Chat
                    </a>
                </li>
                <li>
                    <a href="#payment">
                        <i class="iconsmind-Space-Needle"></i> Payment
                    </a>
                </li>
                <li>
                    <a href="#verifications">
                        <i class="iconsmind-Three-ArrowFork"></i> Verifications
                    </a>
                </li>
                <li>
                    <a href="#profile">
                        <i class="iconsmind-Three-ArrowFork"></i> Profile
                    </a>
                </li>
                <li>
                    <a href="#myTasks">
                        <i class="iconsmind-Three-ArrowFork"></i> My Tasks
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="sub-menu">
        <div class="scroll">
            <ul class="list-unstyled" data-link="payment">
                <li>
                    <a href="Layouts.List.html">
                        <i class="simple-icon-credit-card"></i> Payment Methods
                    </a>
                </li>
                <li>
                    <a href="Layouts.List.html">
                        <i class="simple-icon-credit-card"></i> Payment History
                    </a>
                </li>
            </ul>
            <ul class="list-unstyled" data-link="profile">
                <li>
                    <a href="Apps.MediaLibrary.html">
                        <i class="simple-icon-picture"></i> Change Password
                    </a>
                </li>
                <li>
                    <a href="Apps.Todo.List.html">
                        <a href="{{ url('user/qualification') }}"> <i class="simple-icon-check"></i> Qualifications </a>
                    </a>
                </li>
                <li>
                    <a href="Apps.Survey.List.html">
                        <i class="simple-icon-calculator"></i> Portfolio
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
